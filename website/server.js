var express = require('express');
var app = express();
var exphbs = require('express-handlebars');// moteur de template
const bodyParser = require('body-parser') // To receive POST values in Express, you first need to include the body-parser middleware, which exposes submitted form values on req.body in your route handlers

const FilmService = require('./services/FilmService')

// init handlebars
app.engine('handlebars', exphbs({defaultLayout: 'main'}));// on définit la view "main" comme étant la view de base du template
app.set('view engine', 'handlebars');// Moteur de template

app.use(express.static('public')); // init du fichier static pour le css et les images

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

///////// routing ///////////

// CLIENT PAGES 
app.get('/', function(req, res){
  FilmService.findAll(function(datas){
    res.render('pages/index', {
      title: "Home",
      datas: datas.films
      });
  })   
});

app.get('/films/:_id', function(req, res){
  let id = req.params._id
  FilmService.findOne(id, function(film){
    res.render('pages/film', {
       title: film.title,
       datas : film
      });
  })
});

// ADMIN PAGES
// route admin dashboard
app.get('/admin', function(req, res){
  FilmService.findAll(function(datas){
    res.render('admin/index', {
      title: "Admin Dashboard",
      datas: datas.films
      });
  })
});

// route affichage page nouveau film
app.get('/admin/create', function(req, res){
  res.render('admin/create', { title: "Create Film" });
});

// route store new film
app.post('/admin/store',function(req,res){
  FilmService.create(req.body, function(datas){
    res.redirect('/admin')
  })
})

// route affichage page edition d'un film
app.get('/admin/edit/:_id', function(req, res){
  let id = req.params._id
  FilmService.findOne(id, function(film){
    res.render('admin/edit', {
       title: "Edit Film",
       datas : film,
       release_date : film.release_date.slice(0,10) // necessaire pour avoir le bon format de date XXXX-XX-XX 
      });
  })
});

// route update un film
app.post('/admin/update/:_id',function(req,res){
  let id = req.params._id
  FilmService.update(id, req.body, function(datas){
    res.redirect('/admin')
  })
})

// route affichage page confirmation suppression d'un film
app.get('/admin/delete/:_id', function(req, res){
  let id = req.params._id
  FilmService.findOne(id, function(film){
    res.render('admin/delete', {
       title: "Delete Film",
       datas : film
      });
  })
});

// route delete un film
app.post('/admin/destroy/:_id',function(req,res){
  let id = req.params._id
  FilmService.delete(id, function(datas){
    res.redirect('/admin')
  })
})


// 404 page
app.use(function(req, res, next){
    res.status(404);
    // respond with html page
    if (req.accepts('html')) {
      res.render('pages/404', { url: req.url });
      return;
    }
});


var port = 3000;
app.listen(port);
console.log(`Cinefilms listening on port ${port} `)
const express = require('express')
const app = express()
const FilmRoutes = require('./routes/FilmRoutes')

const bodyParser = require('body-parser');
app.use(bodyParser.json())

// welcome pages
app.get('/api', function (req, res) {
    res.json({"welcome" : "Cinefilms REST API "});
})

// Film routes
app.use('/api', FilmRoutes);

app.listen(3001, function () {
  console.log('API listening on port 3001!')
})
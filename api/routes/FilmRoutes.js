const express = require('express');
const router = express.Router();
const FilmController = require('../controllers/FilmController');

// Production Film Routes
    router.post('/films', FilmController.create);
    router.get('/films', FilmController.findAll);
    router.get('/films/:id', FilmController.findOne);
    router.put('/films/:id', FilmController.update);
    router.delete('/films/:id', FilmController.delete);

module.exports = router;
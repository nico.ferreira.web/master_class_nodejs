const Film = require('../models/FilmModel');


module.exports = {
  // Create and Save a new Film
  create: function(req, res) {
    
    var new_film = new Film({
        title: req.body.title,
        release_date: req.body.release_date,
        image_path: req.body.image_path,
        poster_path: req.body.poster_path,
        description: req.body.description
    })
    
    new_film.save(function (error) {
      if (error) {
        console.log(error)
      }
      res.send({
        success: true,
        message: new_film.title + ' saved successfully!',
        data: new_film
      })
    })
  },
  
  // Retrieve and return all film from the database.
  findAll: function(req, res) {
    Film.find({}, function (error, films) {
      if (error) { console.error(error); }
      res.send({
        films: films
      })
    }).sort({release_date:-1})
  },
  
  // Find a single film with a filmId
  findOne: function(req, res) {
    Film.findById(req.params.id, function (error, film) {
      if (error) { console.error(error); }
      res.send(film)
    })
  },
  
  // Update a film identified by the filmId in the request
  update: function(req, res) {
    Film.findById(req.params.id, function (error, film) {
      if (error) { console.error(error); }
      
        film.title = req.body.title,
        film.release_date = req.body.release_date,
        film.image_path = req.body.image_path,
        film.poster_path = req.body.poster_path,
        film.description = req.body.description
      film.save(function (error) {
        if (error) {
          console.log(error)
        }
        res.send({
          success: true,
          message: 'film updated',
          data: film
        })
      })
    })
  },
  
  // Delete a film with the specified filmId in the request
  delete: function(req, res) {
    Film.deleteOne({
      _id: req.params.id
    }, function(err, film){
      if (err)
      res.send(err)
      res.send({
        success: true,
        message: 'Film deleted',
        data: film
      })
    })
  },
  
}
